export const loadState = (name:string) => {
  try {
    const serializedState = localStorage.getItem('state_'+name);
    if (serializedState === null) {
      return undefined;
    }
    return JSON.parse(serializedState);
  } catch (err) {
    return undefined;
  }
}; 

export const saveState = (name:string,state:any) => {
  try {
    const serializedState = JSON.stringify(state);
    localStorage.setItem('state_'+name, serializedState);
  } catch {
    // ignore write errors
  }
};