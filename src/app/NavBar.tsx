import React from "react";
import { NavLink } from "react-router-dom";
import styles from './Navbar.module.css'

export default function NavBar() {
  return (
    <nav className={styles.NavBar}>
      <NavLink to="/" exact activeClassName={styles.active}>
        Game
      </NavLink>
      <NavLink to="/question-manager" exact activeClassName={styles.active}>
        Question manager
      </NavLink>
    </nav>
  );
}