import React from 'react';
import styles from "./Errors.module.css"
export default function Errors({ errors }: { errors: string[] }) {
  return (
    <div className={styles.errors}>
      {errors.map((message, ind) => <div key={ind}>{message}</div>)}
    </div>
  )
}