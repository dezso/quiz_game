import { configureStore, ThunkAction, Action, getDefaultMiddleware } from '@reduxjs/toolkit';
import questionsReducer, { QUESTIONS_SLICE_NAME } from '../features/QuestionManager/questionsSlice';
import gameReducer from '../features/Game/gameSlice';
import logger from "redux-logger";
import { saveState } from './localstorage';
import throttle from 'lodash.throttle';

export const store = configureStore({
  reducer: {
    questions: questionsReducer,
    game: gameReducer,
  },
  middleware: [
    ...getDefaultMiddleware(),
    logger
  ]
});

store.subscribe(
  throttle(() => {
    saveState(
      QUESTIONS_SLICE_NAME,
      store.getState().questions
    );
  }, 1000)
);

export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
