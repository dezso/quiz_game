import React from 'react';
import './App.css';
import { QuestionManager } from './features/QuestionManager/QuestionManager';
import Game from './features/Game/Game';
import { Router, Switch, Route } from 'react-router';
import NavBar from './app/NavBar';
import { createBrowserHistory } from 'history';

const history = createBrowserHistory();

function App() {
  return (
    <Router history={history}>
      <NavBar />
      <Switch>
        <Route exact path="/">
          <Game />
        </Route>
        <Route exact path="/question-manager">
          <QuestionManager />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
