import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../app/store';
import { loadState } from '../../app/localstorage';


export const QUESTIONS_SLICE_NAME = 'questions';

export interface Question {
  id: number,
  question: string,
  correctAnsver: string
  wrongAnsvers: string[],
}

interface QuestionsState {
  maxId: number;
  questions: Question[];
}

const persistedState = loadState(QUESTIONS_SLICE_NAME);

const initialState: QuestionsState = {
  maxId: 0,
  questions: [],
  ...persistedState
};


export const questionsSlice = createSlice({
  name: QUESTIONS_SLICE_NAME,
  initialState,
  reducers: {
    addQuestion: (state, action: PayloadAction<Question>) => {
      // Immer js handles the immutability
      const newQuestion = { ...action.payload, id: state.maxId };
      state.maxId += 1;
      state.questions = [...state.questions, newQuestion]
    },
    removeQuestionById: (state, action: PayloadAction<number>) => {
      state.questions = state.questions.filter(q => q.id !== action.payload)
    }
  },
});

export const { addQuestion, removeQuestionById } = questionsSlice.actions;

export const selectQuestions = (state: RootState) => state.questions;

export default questionsSlice.reducer;