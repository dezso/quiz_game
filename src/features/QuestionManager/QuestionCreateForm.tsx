import React, { useState, FormEvent } from 'react';

import { Question } from "./questionsSlice";
import _ from 'lodash'
import Errors from '../../app/Errors';
import styles from './QuestionCreateForm.module.css'

export default function QuestionCreateForm({ addQuestion }: { addQuestion: (q: Question) => void }) {
  const defaultState = { question: "", a1: "", a2: "", a3: "", a4: "" };
  const defaultErrors: string[] = [];
  const [state, setState] = useState(defaultState);
  const [errors, seterrors] = useState(defaultErrors);

  const submitHandler = function (e: FormEvent<HTMLFormElement>) {
    e.preventDefault();
    if (sanityCheck()) {
      const newQuestion = {
        id: 0,
        question: state.question,
        correctAnsver: state.a1,
        wrongAnsvers: [state.a2, state.a3, state.a4]
      }
      addQuestion(newQuestion);
      setState(defaultState);
    }
  }

  const sanityCheck = () => {
    const newErrors = []
    if (state.question === "" || state.a1 === "" || state.a2 === "" || state.a3 === "" || state.a4 === "") {
      newErrors.push("Neither the question nor any ansver can be empty");
    }
    if (_.uniq([state.a1, state.a2, state.a3, state.a4]).length !== 4) {
      newErrors.push("Two ansvers can't be the same");
    }
    seterrors(newErrors);
    return newErrors.length === 0;
  }

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const target = event.target;
    setState({ ...state, [target.name]: target.value })
  }

  return (
    <div>
      <Errors errors={errors} />
      <form onSubmit={submitHandler} className={styles.questionCreateForm}>
        <label htmlFor="question">Question</label>
        <input
          name="question"
          type="text"
          value={state.question}
          onChange={handleInputChange} />
        <label htmlFor="a1">Correct ansver</label>
        <input
          name="a1"
          type="text"
          value={state.a1}
          onChange={handleInputChange} />
        <label htmlFor="a2">Wrong ansver 1</label>
        <input
          name="a2"
          type="text"
          value={state.a2}
          onChange={handleInputChange} />
        <label htmlFor="a3">Wrong ansver 2</label>
        <input
          name="a3"
          type="text"
          value={state.a3}
          onChange={handleInputChange} />

        <label htmlFor="a4">Wrong ansver 3</label>
        <input
          name="a4"
          type="text"
          value={state.a4}
          onChange={handleInputChange} />
        <button type="submit">Save question</button>
      </form>
    </div>
  )
}