import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { selectQuestions, addQuestion, removeQuestionById, Question } from './questionsSlice'
import QuestionListItem from './QuestionListItem';
import QuestionCreateForm from './QuestionCreateForm';
import styles from './QuestionManager.module.css'

export function QuestionManager() {
  const dispatch = useDispatch();
  const { questions } = useSelector(selectQuestions);


  return (<div className={styles.questionManager}>
    <div>
      <h1>Question Manager</h1>
      <ol className={styles.questionList}>
        {questions.map((question) =>
          <QuestionListItem
            key={question.id}
            question={question}
            remove={() => dispatch(removeQuestionById(question.id))}
          />
        )}
      </ol>
    </div>
    <QuestionCreateForm addQuestion={(q: Question) => dispatch(addQuestion(q))} />
  </div >)

}