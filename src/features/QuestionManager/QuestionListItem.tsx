import React from 'react';

import { Question } from "./questionsSlice";
import styles from './QuestionListItem.module.css'

export default function QuestionListItem({ question, remove }: { question: Question, remove: Function }) {
  const possibleAnsvers = [question.correctAnsver, ...question.wrongAnsvers]
  return <li>
    <div>{question.question}</div>
    <ul className={styles.ansverList}>
      {possibleAnsvers.map((ansver, ind) =>
        <li key={ind} className={ind === 0 ? styles.correct : ""}>{ansver}</li>
      )}
    </ul>
    <button onClick={() => remove()}>remove</button>
  </li>
}