import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import { selectGame, STATES, startGame, nextQuestion, newGame } from './gameSlice'
import ShowQuestion from './ShowQuestion';
import { selectQuestions } from '../QuestionManager/questionsSlice';
import styles from "./Game.module.css"
import Errors from '../../app/Errors';


export default function Game() {
  const dispatch = useDispatch();
  const questionSelector = useSelector(selectQuestions)
  const originalQuestions = questionSelector.questions;
  const { points, state, playerName, errors } = useSelector(selectGame);
  const [name, setName] = useState("");



  const content = {
    [STATES.PENDING]: (
      <div>
        <label htmlFor="name">Name:</label>
        <input type="text" name="name" value={name} onChange={(e) => setName(e.target.value)} />
        <br />
        <button
          className={styles.button}
          onClick={() => { dispatch(startGame({ questions: originalQuestions, name: name })); dispatch(nextQuestion()) }}
          disabled={name.length === 0}
        >Start</button>
      </div>
    ),
    [STATES.INPROGRESS]: (
      <div>
        <div className={styles.gameStatus}><span>Player: {playerName}</span> <span>Points: {points}</span></div>
        <ShowQuestion />
      </div>
    ),
    [STATES.ENDED]: (
      <div>
        Congratulation! You have earned {points} points
        <button
          onClick={() => { dispatch(newGame()); }}
        >New Game</button>
      </div>
    )
  }

  return (
    <div className={styles.game}>
      <h1>Quiz Game</h1>
      <Errors errors={errors} />
      {content[state]}

    </div>
  );
}