import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../app/store';
import { Question } from '../QuestionManager/questionsSlice';
// import { loadState } from '../../app/localstorage';



export const GAME_SLICE_NAME = 'game';
const ERROR_NO_QUESTION = "There is no question set in the game"

export const STATES = {
  PENDING: 0,
  INPROGRESS: 1,
  ENDED: 2
}

export interface Game {
  errors: string[],
  points: number,
  state: number,
  questions?: Question[],
  currentQuestion?: Question,
  currentPossibleAnsvers?: string[],
  givenAnsver: string,
  playerName?: string
}



// const persistedState=loadState(GAME_SLICE_NAME);

const initialState: Game = {
  points: 0,
  state: STATES.PENDING,
  errors: [],
  givenAnsver: "",
};


export const gameSlice = createSlice({
  name: GAME_SLICE_NAME,
  initialState,
  reducers: {
    setName: (state, action: PayloadAction<string>) => {
      state.playerName = action.payload
    },
    startGame: (state, action: PayloadAction<{ questions: Question[], name: string }>) => {
      state.errors = state.errors.filter(e => e !== ERROR_NO_QUESTION);
      if (action.payload.questions.length === 0) {
        state.errors.push(ERROR_NO_QUESTION);
        return;
      }
      state.points = 0;
      state.state = STATES.INPROGRESS;
      state.questions = action.payload.questions;
      state.playerName = action.payload.name;
    },
    nextQuestion: (state) => {
      const question = state.questions?.[0];
      state.currentQuestion = question;
      state.questions = state.questions?.slice(1);
      state.givenAnsver = "";
      if (question) {
        state.currentPossibleAnsvers = [question.correctAnsver, ...question.wrongAnsvers]
          .sort(() => .5 - Math.random());
        //Not the best solution but it works.
      }
    },
    endGame: (state) => {
      state.state = STATES.ENDED;
    },
    newGame: (state) => {
      state.state = STATES.PENDING;
    },
    setAnsver: (state, action: PayloadAction<string>) => {
      state.givenAnsver = action.payload;
      if (action.payload === state.currentQuestion?.correctAnsver) {
        state.points += 1;
      }
    }
  },
});

export const { setName, startGame, nextQuestion, endGame, setAnsver, newGame } = gameSlice.actions;

export const selectGame = (state: RootState) => state.game;

export default gameSlice.reducer;