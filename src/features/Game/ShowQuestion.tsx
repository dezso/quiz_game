import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { selectGame, setAnsver, nextQuestion, endGame } from './gameSlice';
import styles from "./ShowQuestion.module.css"

export default function ShowQuestion() {
  const dispatch = useDispatch();
  const { currentPossibleAnsvers, givenAnsver, currentQuestion, questions } = useSelector(selectGame);
  const [selected, setSelected] = useState(givenAnsver);

  const ContinueButton = () => {
    if (givenAnsver === "") {
      return <button className={styles.button} onClick={() => dispatch(setAnsver(selected))} disabled={selected === ""}>Ansver</button>
    } else if (questions && questions.length > 0) {
      return <button className={styles.button} onClick={() => {setSelected("");dispatch(nextQuestion())}} >Next</button>
    } else {
      return <button className={styles.button} onClick={() => dispatch(endGame())} >End</button>
    }
  }
  const labelClass = (ansver: string) => {
    if (givenAnsver === "") {
      return selected === ansver ? styles.selected : "";
    } else {
      if (ansver === currentQuestion?.correctAnsver) {
        return styles.correct
      } else if (ansver === givenAnsver) {
        return styles.incorrect
      }
    }
    return "";
  }

  return <div>
    <h2>{currentQuestion?.question}</h2>
    <div className={styles.ansverContainer}>
      {currentPossibleAnsvers?.map((ansver, ind) => {
        return (
          <label key={ind} className={labelClass(ansver) + " " + styles.label}>
            {ansver}
            <input
              className={styles.hidden}
              type="radio" name="ansver" value={ansver}
              onChange={(e) => setSelected(e.target.value)}
              checked={selected === ansver}
              disabled={givenAnsver !== ""}
            />
          </label>
        )
      })}
    </div>
    <ContinueButton />
  </div>
}